#ifndef TEST_H_
#define TEST_H_

#define INPUT_NEURONS 784
#include <vector>
#include "Gpu_matrix.h"

class Test {
 public:
    Test(std::vector<float> &, short, size_t);
    Test(const Test &);
    ~Test();
    const Gpu_matrix get_data() const;
    short get_answer() const;
    size_t get_size() const;
    size_t get_number() const;
 private:
    Gpu_matrix data;
    size_t number;
    short answer;
};


#endif //TEST_H_
