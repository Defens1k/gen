#ifndef COUNTING_SEMAPHORE_H_
#define COUNTING_SEMAPHORE_H_
#include <semaphore.h>
class Counting_semaphore {
 public:
     Counting_semaphore();
     ~Counting_semaphore();
     Counting_semaphore(int);
     Counting_semaphore(int, int);
     void wait();
     void signal();
 private:
     sem_t semaphore;
};

#endif //COUNTING_SEMAPHORE_H_
