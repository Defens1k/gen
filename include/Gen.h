#ifndef GEN_H_
#define GEN_H_
#include "Test.h"
#include <iostream>
#include <cstdlib>
#include "Gpu_matrix.h"
#define LAYERS 2
#define NEURONS 128
#define INPUTS 784
#define ANSWERS 10
class Gen {
 public:
    Gen();
    //Gen(const Gen &);
    void set_score(float);
    float get_score() const;
    bool operator> (const Gen&);
    bool operator< (const Gen&);
    short predict(const Test &);
 private:
    float score;
    Gpu_matrix A;
    Gpu_matrix B;
    Gpu_matrix p1;
    Gpu_matrix p1a;
    Gpu_matrix p2;
};
#endif //GEN_H_
