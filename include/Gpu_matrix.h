#ifndef GPU_MATRIX_H_
#define GPU_MATRIX_H_
#include <cublas.h>
#include <iostream>
class Gpu_matrix {
 public:
    Gpu_matrix(const Gpu_matrix &);
    Gpu_matrix(float * , size_t, size_t);
    Gpu_matrix(size_t, size_t);
    ~Gpu_matrix();
    Gpu_matrix & operator=(const Gpu_matrix &);
    Gpu_matrix  operator* (const Gpu_matrix&) const;
    float * get_gpu_data() const ;
    float * get_memory_data() const ;
    void set(size_t, size_t, float);
    float get(size_t, size_t);
    size_t N() const;
    size_t M() const ;
    cublasStatus_t status;
    void check_status(std::string);
 private:
    void sync_gpu_to_memory();
    float * memory_data;
    float * gpu_data;
    size_t n, m;
};
#endif //GPU_MATRIX_H_
