#ifndef TEST_ROOM_H_
#define TEST_ROOM_H_
#include <vector>
#include <thread>
#include <mutex>
#include <iostream>
#include "Gen.h"
#include "Apparat.h"
#include "Counting_semaphore.h"
#include "Test_creator.h"
#include "Population.h"
#define TRAIN_APPARATS_COUNT 20
#define TEST_APPARATS_COUNT 5
class Test_room {
 public:
     Test_room();
     ~Test_room();
     Population service_gen(Population);
 private:
    Test_creator printer;
    Apparat * train_apparats;
    Apparat * test_apparats;

    Counting_semaphore work;
    std::thread thr;
    void loop();
    bool loop_destructor;
};
#endif //TEST_ROOM_H_
