#ifndef APPARAT_H_
#define APPARAT_H_

#include <vector>
#include <thread>
#include <queue>
#include <mutex>

#include <iostream>

#include "Population.h"
#include "Test.h"
#include "Gen.h"
#include "Counting_semaphore.h"
class Apparat {
 public:
     Apparat(const std::vector<Test> *);
     Apparat();
     ~Apparat();
     void set_tests(const std::vector<Test> *);
     void enqueue(std::vector<Gen>);
     std::vector<Gen> return_when_free();
 private:
     void loop();
     float evaluate(short, short);
     std::thread thr;

     const std::vector<Test> * tests;
     std::mutex tests_mutex;

     std::queue<Gen> queue;
     std::mutex queue_mutex;

     std::vector<Gen> return_vector;
     std::mutex return_mutex;

     Counting_semaphore work;

     bool loop_destructor;
};


#endif //APPARAT_H_
