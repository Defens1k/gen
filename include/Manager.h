#ifndef MANAGER_H_
#define MANAGER_H_
#define BEGIN_POPULATION_SIZE 2000

#include <iostream>
#include <thread>
#include <cublas.h>

#include "Population.h"
#include "Gen.h"
#include "Selection_room.h"
#include "Test_room.h"
#include "Log_room.h"
class Manager {
 public:
     Manager();
     ~Manager();

 private:
    Test_room test_room;
    Selection_room selection_room;
    Log_room log_room;
    Population population;

    std::thread thr;
    Counting_semaphore work;
    void loop();
    bool loop_destructor;
};
#endif //MANAGER_H_
