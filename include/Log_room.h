#ifndef LOG_ROOM_H_
#define LOG_ROOM_H_
#include "Population.h"
#include "Test_creator.h"
#include "Counting_semaphore.h"
#include <mutex>
#include <thread>

class Log_room {
 public:
    Log_room();
    ~Log_room();
    void logging(Population);

 private:
    
    Test_creator printer;

    std::mutex logger_mutex;
    Population population;
    Counting_semaphore work;
    std::thread thr;
    void loop();
    bool loop_destructor;
};

#endif //LOG_ROOM_H_
