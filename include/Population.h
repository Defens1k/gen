#ifndef POPULATION_H_
#define POPULATION_H_
#include <vector>
#include <algorithm>
#include "Gen.h"
#define SORT_THREADS_COUNT 32
class Population : public std::vector<Gen> {
 public:
    Population(): epoch(0){std::cout << "Population constructor\n";};
    Population(size_t init_epoch) : epoch(init_epoch){std::cout << "Population constructor size_t epoch\n";};
    Population(std::vector<Gen> init_vector, size_t init_epoch) : std::vector<Gen>(init_vector), epoch(init_epoch){};
    void sort();
    size_t get_epoch() const {return epoch;};
    void increment_epoch() {epoch++;};
 private:
    size_t epoch;
    std::vector<Gen> gens;

};

#endif //POPULATION_H_
