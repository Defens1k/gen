#ifndef TEST_CREATOR_H_
#define TEST_CREATOR_H_
#include <vector>
#include "Test.h"
#include<iostream>
#include<fstream>
class Test_creator {
 public:
    static Test_creator & get_instance();
    const std::vector<Test> * get_train_tests() const;
    const std::vector<Test> * get_test_tests() const;
 private:
    Test_creator();
    void read_train_x();
    void read_test_x();
    void read_train_y();
    void read_test_y();
    void create_tests();
    std::vector<std::vector<float>> train_x_float;
    std::vector<std::vector<float>> test_x_float;
    std::vector<short> train_y_short;
    std::vector<short> test_y_short;
    std::vector<Test> train_tests;
    std::vector<Test> test_tests;
};
#endif //TEST_CREATOR_H_
