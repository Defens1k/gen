#include "Test_creator.h"



Test_creator & Test_creator::get_instance() {
    static Test_creator instance;
    return instance;
}

Test_creator::Test_creator() {
    read_train_x();
    read_test_x();
    read_train_y();
    read_test_y();

    create_tests();
    
}

const std::vector<Test> * Test_creator::get_train_tests() const{
    return &train_tests;
}

const std::vector<Test> * Test_creator::get_test_tests() const{
    return &test_tests;
}

void Test_creator::create_tests() {
    for (size_t i = 0; i < 60000; i++) {
        std::cout << i << std::endl;
        train_tests.push_back(Test(train_x_float[i], train_y_short[i], i));
    }
    for (size_t i = 0; i < 10000; i++) {
        test_tests.push_back(Test(test_x_float[i], test_y_short[i], 60000 + i));
        std::cout << i << std::endl;
    }
}

void Test_creator::read_test_x() {
    std::ifstream file;
    file.open("raw_data/test_x_byte");
    char * buffer = new char[28 * 28];
    file.read(buffer, 4 * 4);
    for (size_t i = 0; i < 10000; i++) {
        file.read(buffer, 28 * 28);
        std::vector<float> img = std::vector<float>();
        for (int j = 0; j < 28 * 28; j++) {
            unsigned char c = buffer[j];
            img.push_back(c);
        }
        test_x_float.push_back(img);
    }
    delete [] buffer;
}

void Test_creator::read_train_x() {
    std::ifstream file;
    file.open("raw_data/train_x_byte");
    char * buffer = new char[28 * 28];
    file.read(buffer, 4 * 4);
    for (size_t i = 0; i < 60000; i++) {
        file.read(buffer, 28 * 28);
        std::vector<float> img = std::vector<float>();
        for (int j = 0; j < 28 * 28; j++) {
            unsigned char c = buffer[j];
            img.push_back(c);
        }
        train_x_float.push_back(img);
    }
    delete [] buffer;
}

void Test_creator::read_train_y() {
    std::ifstream file;
    file.open("raw_data/train_y_byte");
    char * buffer = new char[60000];
    file.read(buffer, 2 * 4);
    file.read(buffer, 60000);
    for (size_t i = 0; i < 60000; i++) {
        unsigned char c = buffer[i];
        train_y_short.push_back(c);
    }
    delete [] buffer;
}

void Test_creator::read_test_y() {
    std::ifstream file;
    file.open("raw_data/test_y_byte");
    char * buffer = new char[10000];
    file.read(buffer, 2 * 4);
    file.read(buffer, 10000);
    for (size_t i = 0; i < 10000; i++) {
        unsigned char c = buffer[i];
        test_y_short.push_back(c);
    }
    delete [] buffer;
}



