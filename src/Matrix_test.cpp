#include <iostream>
#include <cublas.h>
#include "Gpu_matrix.h"

int main() {
    if(cublasInit() == CUBLAS_STATUS_NOT_INITIALIZED) {
        printf("CUBLAS init error.\n");
        return -1;
    }
    float * a = new float[6];
    a[0] = 1;
    a[1] = 2;
    a[2] = 3;
    a[3] = 4;
    a[4] = 5;
    a[5] = 6;

    float * b = new float[3];
    b[0] = 1;
    b[1] = 2;
    b[2] = 3;

    Gpu_matrix A(a, 3, 2);

    Gpu_matrix B(b, 1, 3);

//    Gpu_matrix C = B * A;

    std::cout << b[0] << " " << b[1] << " " << b[2] << std::endl;
//    std::cout << C.get_memory_data()[0] << " " << C.get_memory_data()[1] << std::endl;



    cublasShutdown();
    return 0;


}
