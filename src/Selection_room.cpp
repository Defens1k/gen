#include "Selection_room.h"
Selection_room::Selection_room() {
    std::cout << "Selection_room constructor\n";
}

Population Selection_room::select(Population input_gens) {
    input_gens.increment_epoch();
    return input_gens;
}
