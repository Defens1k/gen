#include "Test_room.h"

Test_room::Test_room(): printer(Test_creator::get_instance()) {
    train_apparats = new Apparat[TRAIN_APPARATS_COUNT];
    test_apparats = new Apparat[TEST_APPARATS_COUNT];
    for (int i = 0; i < TRAIN_APPARATS_COUNT; i++) {
        train_apparats[i].set_tests(printer.get_train_tests());
    }
    for (int i = 0; i < TEST_APPARATS_COUNT; i++) {
        test_apparats[i].set_tests(printer.get_test_tests());
    }
    loop_destructor = false;
    thr = std::thread([this](){loop();});
}

Test_room::~Test_room() {
    if (train_apparats != nullptr) {
        delete [] train_apparats;
        train_apparats = nullptr;
    }
    if (test_apparats != nullptr) {
        delete [] test_apparats;
        test_apparats = nullptr;
    }
    loop_destructor = true;
    work.signal();
    thr.join();
}

Population Test_room::service_gen(Population population) {
    if (population.size() < 40) {
        std::cerr << "error: Test_room input gens vector is to small!\n";
    }
    int prev_border = 0;
    for (int i = 1; i <= TRAIN_APPARATS_COUNT; i++) {
        size_t current_border = 0;
        if (i == TRAIN_APPARATS_COUNT) {
            current_border = population.size();
        } else {
            current_border = population.size() / TRAIN_APPARATS_COUNT * i;
        }
        train_apparats[i - 1].enqueue(std::vector<Gen>(population.begin() + prev_border, population.begin() + current_border));
        prev_border = current_border;
    }
    population.clear();
    for (int i = 0; i < TRAIN_APPARATS_COUNT; i++) {
        std::vector<Gen> local = train_apparats[i].return_when_free();
        population.insert(population.end(), local.begin(), local.end());
    }
    std::cout << "end service gen\n";
    return population;
}



void Test_room::loop() {
    while(1) {
        work.wait();
        if (loop_destructor == true) {
            return;

        }





    }
}
