#include "Counting_semaphore.h"
Counting_semaphore::Counting_semaphore() {
    sem_init(&semaphore, 0, 0);
}

Counting_semaphore::Counting_semaphore(int value) {
    sem_init(&semaphore, 0, value);
}

Counting_semaphore::Counting_semaphore(int value, int pshared) {
    sem_init(&semaphore, pshared, value);
}

Counting_semaphore::~Counting_semaphore() {
    sem_destroy(&semaphore);
}

void Counting_semaphore::wait() {
    sem_wait(&semaphore);
}

void Counting_semaphore::signal() {
    sem_post(&semaphore);
}
