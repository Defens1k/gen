#include "Manager.h"

Manager::Manager() : population(1) {
    std::cout << "Manager constructor: BEGIN POPULATION_SIZE = " << BEGIN_POPULATION_SIZE << std::endl;;
    cublasInit();
    for (size_t i = 0; i < BEGIN_POPULATION_SIZE; i++) {
        population.push_back(Gen());
    }
    loop_destructor = false;
    thr = std::thread([this](){loop();});
    work.signal();
}

Manager::~Manager() {
    loop_destructor = true;
    work.signal();
    thr.join();
    cublasShutdown();
}

void Manager::loop() {
    while(1) {
        work.wait();
        if (loop_destructor == true) {
            return;

        }
        auto testing_population = test_room.service_gen(population);
        testing_population.sort();
        log_room.logging(testing_population);
        population = selection_room.select(testing_population);
        work.signal();
    }
}
