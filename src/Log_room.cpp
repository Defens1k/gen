#include "Log_room.h"

Log_room::Log_room() : printer(Test_creator::get_instance()){
    loop_destructor = false;
    //logger_m
    thr = std::thread([this]() {loop();});   
}

Log_room::~Log_room() {
    loop_destructor= true;
    work.signal();
    thr.join();
}

void Log_room::logging(Population input_population) {
    if (logger_mutex.try_lock() == false) {
        std::cerr << "logger is locked! skip logg population\n";
        return;
    }
    population = input_population;
    work.signal();
    logger_mutex.unlock();
}

void Log_room::loop() {
    while(1) {
        work.wait();
        std::cout << "log room loop\n";
        if (loop_destructor == true) {
            return;
        }
        logger_mutex.lock();
        population.sort();
        std::cout << "Epoch is: " << population.get_epoch() << std::endl;
        std::cout << "Best   Gen have score: " << population[0].get_score() << std::endl;
        std::cout << "second Gen have score: " << population[1].get_score() << std::endl;
        std::cout << "100    Gen have score: " << population[99].get_score() << std::endl;
        std::cout << "1000    Gen have score: " << population[999].get_score() << std::endl;
        logger_mutex.unlock(); 
    }
}
