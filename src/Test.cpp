#include "Test.h"

Test::Test(std::vector<float> & test_data, short test_answer, size_t test_number) : data(Gpu_matrix(test_data.data(), 1, test_data.size())) { 
    number = test_number;
    answer = test_answer;
}

Test::Test(const Test & original) : data(original.get_data()) {
    number = original.get_number();
    answer = original.get_answer();
}

Test::~Test() {
}

const Gpu_matrix Test::get_data() const{
    return data;
}

short Test::get_answer() const {
    return answer;
}

size_t Test::get_size() const {
    return data.M();
}

size_t Test::get_number() const {
    return number;
}
