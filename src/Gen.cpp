#include "Gen.h"

Gen::Gen() :
    A(INPUTS, NEURONS),
    B(NEURONS, ANSWERS),
    p1(1, NEURONS),
    p1a(1, NEURONS),
    p2(1, ANSWERS),
    score(-1)
{
    std::cout << "Gen constructor\n";
    for (size_t i = 0; i < NEURONS; i++) {
        for (size_t j = 0; j < INPUTS; j++) {
            A.set(j, i, std::rand());
        }
        for (size_t j = 0; j < ANSWERS; j++) {
            B.set(i, j, std::rand());
        }
    }
}


void Gen::set_score(float input_score) {
    score = input_score;
}

float Gen::get_score() const{
    return score;
}

short Gen::predict(const Test & test) {
    p1 =  test.get_data() * A;
    p2 = p1 * B;
    short result = 0;
    float max = p2.get(0, 0);
    for (short i = 1; i < ANSWERS; i++) {
        if (p2.get(0, i) > max) {
            max = p2.get(0, i);
            result = i;
        }
    }
 //   std::cout << "predict\n";
    return result;
}

bool Gen::operator> (const Gen &g2) {
    return this->get_score() > g2.get_score();
}

bool Gen::operator< (const Gen &g2) {
    return this->get_score() < g2.get_score();
}
