#include "Gpu_matrix.h"

Gpu_matrix::Gpu_matrix(const Gpu_matrix & original_matrix) {
    if (this == &original_matrix) {
        std::cerr << "gpu matrix error\n";
    }

    n = original_matrix.N();
    m = original_matrix.M();

    memory_data = (float *) malloc (n * m * sizeof(float));

    status = cublasAlloc(n * m, sizeof(float),(void **) &gpu_data);
    check_status("alloc");


    status = cublasGetMatrix(n, m, sizeof(float), original_matrix.get_gpu_data(), n, memory_data, n);
    check_status("get");


    status = cublasSetMatrix(n, m, sizeof(float), memory_data, n, gpu_data, n);
    check_status("set");
}

void Gpu_matrix::check_status(std::string msg) {
    if (status == CUBLAS_STATUS_SUCCESS) {
        return;
    }
    std::cerr << msg << " ";
    if (status == CUBLAS_STATUS_INVALID_VALUE) {
        std::cerr << "CUBLAS_STATUS_INVALID_VALUE\n";
        return;
    }
    if (status == CUBLAS_STATUS_MAPPING_ERROR) {
        std::cerr << "CUBLAS_STATUS_MAPPING_ERROR\n";
        return;
    }
    std::cerr << "error: status not CUBLAS_STATUS_SUCCES\n";
}

Gpu_matrix::Gpu_matrix(float * input_data, size_t N_, size_t M_) {
    n = N_;
    m = M_;
    memory_data = (float *) malloc (n * m * sizeof(float));
    status = cublasAlloc(n * m, sizeof(float),(void **) &gpu_data);
    check_status("alloc");

    status = cublasSetMatrix(n, m, sizeof(float), input_data, n, gpu_data, n);
    check_status("set");



    status = cublasGetMatrix(n, m, sizeof(float), gpu_data, n, memory_data, n);
    check_status("get");
}

Gpu_matrix & Gpu_matrix::operator= (const Gpu_matrix & original_matrix) {
    if (this == &original_matrix) {
        std::cerr << "gpu matrix error\n";
    }

    free(memory_data);
    cublasFree(gpu_data);

    n = original_matrix.N();
    m = original_matrix.M();

    memory_data = (float *) malloc (n * m * sizeof(float));
    status = cublasGetMatrix(n, m, sizeof(float), original_matrix.get_gpu_data(), n, memory_data, n);
    check_status("get");


    status = cublasAlloc(n * m, sizeof(float),(void **) &gpu_data);
    check_status("alloc");

    cublasSetMatrix(n, m, sizeof(float), memory_data, n, gpu_data, n);
    check_status("set");

    return *this; 

}

Gpu_matrix::Gpu_matrix(size_t N_, size_t M_) {
    n = N_;
    m = M_;
    memory_data = (float *) malloc (n * m * sizeof(float));
    status = cublasAlloc(n * m, sizeof(float),(void **) &gpu_data);
    check_status("alloc");

    status = cublasGetMatrix(n, m, sizeof(float), gpu_data, n, memory_data, n);
    check_status("get");
}

Gpu_matrix::~Gpu_matrix() {
    free(memory_data);
    cublasFree(gpu_data);
}

void Gpu_matrix::set(size_t N, size_t M, float value) {
    if ((N > n - 1) || (M > m - 1)) {
        std::cerr << "set gpu matrix out of range\n";
    }
    memory_data[M * m + N] = value;
    status = cublasSetMatrix(n, m, sizeof(float), memory_data, n, gpu_data, n);
    check_status("set");
}

float Gpu_matrix::get(size_t N, size_t M) {
    return memory_data[N * n + M];
}

Gpu_matrix Gpu_matrix::operator* (const Gpu_matrix& second_matrix) const{
    if (m != second_matrix.N()) {
        std::cerr << "Error: matrix\n" << m << " " << second_matrix.N();
    }
    Gpu_matrix new_matrix = Gpu_matrix(n, second_matrix.M());
    cublasSgemm('n', 'n', n, second_matrix.M(), m, 1.0f, gpu_data, n, second_matrix.get_gpu_data(), m, 1.0f, new_matrix.get_gpu_data(), n);
    new_matrix.sync_gpu_to_memory();
    return new_matrix;
}

void Gpu_matrix::sync_gpu_to_memory() {
    status = cublasGetMatrix(n, m, sizeof(float), gpu_data, n, memory_data, n);
    check_status("get");
}

float * Gpu_matrix::get_gpu_data() const {
    return gpu_data;
}

float * Gpu_matrix::get_memory_data() const {
    return memory_data;
}

size_t Gpu_matrix::N() const {
    return n;
}

size_t Gpu_matrix::M() const{
    return m;
}


