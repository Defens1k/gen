#include "Apparat.h"

Apparat::Apparat(const std::vector<Test> * input_tests) : tests (input_tests) {
    tests_mutex.lock();
    tests_mutex.unlock();
    loop_destructor = false;
    thr = std::thread([this](){loop();});
}

Apparat::Apparat() : tests((std::vector<Test>*)nullptr) {
    loop_destructor = false;
    thr = std::thread([this](){loop();});
}

Apparat::~Apparat() {
    loop_destructor = true;
    work.signal();
    thr.join();
}


void Apparat::set_tests(const std::vector<Test> * input_tests) {
    tests_mutex.lock();
    tests = input_tests;
    tests_mutex.unlock();
}

void Apparat::enqueue(std::vector<Gen> input_gens) {
    std::cout << "Apparat enqueue gens" << input_gens.size() << std::endl;
    queue_mutex.lock();
    for (auto i = input_gens.begin(); i != input_gens.end(); i++) {
        queue.push(*i);
    }
    queue_mutex.unlock();
    work.signal();
}

std::vector<Gen> Apparat::return_when_free() {
    return_mutex.lock();
    queue_mutex.lock();
    std::vector<Gen> value;
    return_vector.swap(value);
    queue_mutex.unlock();
    return_mutex.unlock();
    return value;
}


void Apparat::loop() {
    while(1) {
        work.wait();
        if (loop_destructor == true) {
            return;
        }
        return_mutex.lock();
        queue_mutex.lock();
        while(queue.empty() == false) {
            Gen testing_gen = queue.front();
            queue.pop();
            float score = 0;
            for (auto test = tests->begin(); test != tests->end(); test++) {
                score += evaluate(testing_gen.predict(*test), test->get_answer());
            }
            testing_gen.set_score(score);
            return_vector.push_back(testing_gen);
        }
        queue_mutex.unlock();
        return_mutex.unlock();
    }
}

float Apparat::evaluate(short predicted_value, short original_value) {
    if (predicted_value == original_value) {
        return 1;
    } else {
        return 0;
    }
}
