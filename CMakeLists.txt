cmake_minimum_required(VERSION 3.15)


set (CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -pthread " CACHE STRING "Set C++ Compiler Flags" FORCE)

LINK_DIRECTORIES(
	/usr/local/cuda/targets/x86_64-linux/lib
)

project (Gen)

set(SOURCES
    src/Gpu_matrix.cpp
    src/Test.cpp
    src/Population.cpp
    src/Manager.cpp
    src/Log_room.cpp
    src/Selection_room.cpp
    src/Test_room.cpp
    src/Counting_semaphore.cpp
    src/Test_creator.cpp
    src/Apparat.cpp
    src/Gen.cpp
    src/main.cpp
)

add_executable(gen ${SOURCES})

add_executable(test_matrix  
        src/Matrix_test.cpp
        src/Gpu_matrix.cpp
)

target_include_directories(gen
    PRIVATE 
        ${PROJECT_SOURCE_DIR}/include
        /usr/local/cuda/targets/x86_64-linux/include
)

target_include_directories(test_matrix
    PRIVATE 
        ${PROJECT_SOURCE_DIR}/include
        /usr/local/cuda/targets/x86_64-linux/include
)



target_link_libraries(gen cublas curand)
target_link_libraries(test_matrix cublas curand)
